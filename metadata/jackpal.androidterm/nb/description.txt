Få tilgang til Androids innebygde kommandolinjeskall. Slipp fri din indre nerd!

Dette er en ny versjon av det populære programmet "Android terminalemulator". Samme programmet, bare nytt navn.

Toppfunksjoner

+ Full Linuxterminalemulering. + Flerfoldige vinduer. + Oppstartssnarveier. + UTF-8 -tekst. (Arabisk, Kinesisk, Hebraisk, Japansk, Koreansk, Russisk, Thai, osv.) Helt gratis. Ingen reklame, ingen innebygde kjøpsmuligheter, ingen masing, ingenting.

Rask O-S-S:

Ønsker du å vite mer om Terminalemulator for Android?

Ta del i G+gemenskapen: #Android Terminal Emulator

https://plus.google.com/u/0/communities/106164413936367578283

Eller sjekk ut dokumentasjonswiki-en:

https://github.com/jackpal/Android-Terminal-Emulator/wiki

Ønsker du å forbedre en oversettelse av Terminalemulator for Android? Se https://github.com/jackpal/Android-Terminal-Emulator/wiki/Translating-to-Other-Languages for instruksjoner.

